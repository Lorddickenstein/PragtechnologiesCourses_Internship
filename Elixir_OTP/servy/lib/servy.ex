defmodule Servy do
  use Application

  # start function will be invoked when the application is started such as in iex
  def start(_type, _args) do
    IO.puts("Starting the application... ")
    Servy.Supervisor.start_link()
  end
end
