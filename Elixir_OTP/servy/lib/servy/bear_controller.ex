defmodule Servy.BearController do

  alias Servy.Wildthings
  alias Servy.Bear

  @templates_path Path.expand("../templates", __DIR__)

  # defp bear_item(bear) do
  #   "<li>#{bear.name} - #{bear.type}</li>"
  # end

  defp render(conv, template, bindings \\ []) do
    content =
      @templates_path
      |> Path.join(template)
      |> EEx.eval_file(bindings)

    %{ conv | status: 200, resp_body: content }
  end

  def index(conv) do
    bears =
      Wildthings.list_bears
      # |> Enum.filter(fn(b) -> Bear.is_grizzly(b) end)
      # |> Enum.filter(&Bear.is_grizzly(&1))
      # |> Enum.filter(&Bear.is_grizzly/1)
      # |> Enum.sort(fn(b1, b2) -> Bear.order_asc_by_name(b1, b2) end)
      # |> Enum.sort(&Bear.order_asc_by_name(&1, &2))
      |> Enum.sort(&Bear.order_asc_by_name/2)
      # |> Enum.map(fn(b) -> bear_item(b) end)
      # |> Enum.map(&bear_item(&1))
      # |> Enum.map(&bear_item/1)
      # |> Enum.join

    # IO.inspect(@templates_path)
    render(conv, "index.eex", bears: bears)
  end

  def show(conv, %{"id" => id}) do
    bear = Wildthings.get_bear(id)

    render(conv, "show.eex", bear: bear)
  end

  def create(conv, %{"name" => name, "type" => type} = _params) do
    %{ conv | status: 201, resp_body: "Created a #{type} bear named #{name}!"}
  end
end
