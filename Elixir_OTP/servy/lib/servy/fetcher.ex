defmodule Servy.Fetcher do

  # def async(camera_name) do
  #   parent = self()

  #   spawn(fn -> send(parent, {:result, VideoCam.get_snapshot(camera_name)}) end)
  # end

  # make it more generic by passing in functions
  def async(fun) do
    parent = self()

    spawn(fn -> send(parent, {self(), :result, fun.()}) end)
  end

  def get_result(pid) do
    receive do {^pid, :result, value} -> value end
  end
end
