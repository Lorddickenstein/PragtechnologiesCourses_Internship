defmodule Servy.Handler do
  @moduledoc "Handles HTTP requests."

  alias Servy.Conv
  alias Servy.BearController
  # alias Servy.VideoCam

  @pages_path Path.expand("../pages", __DIR__)

  import Servy.Plugins, only: [rewrite_path: 1, log: 1, track: 1]
  import Servy.Parser, only: [parse: 1]

  @doc "Transforms the request into a response"
  def handle(request) do
    request
    |> parse
    |> rewrite_path
    |> log
    |> route
    |> track
    |> format_response
  end

  def route(%Conv{ method: "GET", path: "/home"} = conv) do
    %{ conv | status: 200, resp_body: "<h1>Hello World</h1>" }
  end

  def route(%Conv{ method: "POST", path: "/pledges" } = conv) do
    Servy.PledgeController.create(conv, conv.params)
  end

  def route(%Conv{ method: "GET", path: "/pledges" } = conv) do
    Servy.PledgeController.index(conv)
  end

  def route(%Conv{ method: "GET", path: "/sensors" } = conv) do

    sensor_data = Servy.SensorServer.get_sensor_data()

    %{ conv | status: 200, resp_body: inspect(sensor_data) }
  end
    # Process.info(pid, :messages) # shows information about a certain process

    # The request-handling process
    # parent = self()

    # pid1 = Fetcher.async(fn -> VideoCam.get_snapshot("cam-1") end),
    # pid2 = Fetcher.async(fn -> VideoCam.get_snapshot("cam-2") end),
    # pid3 = Fetcher.async(fn -> VideoCam.get_snapshot("cam-3") end)

    # pid4 = Fetcher.async(fn -> Servy.Tracker.get_location("bigfoot") end)
    # Task is a module in elixir that returns a Task STRUCT and provides abstraction to processes where the values are to be acquired at a later time
    # task = Task.async(fn -> Servy.Tracker.get_location("bigfoot") end)

    # snapshots =
    #   ["cam-1", "cam-2", "cam-3"]
    #   |> Enum.map(&Task.async(fn -> VideoCam.get_snapshot(&1) end))
    #   |> Enum.map(&Task.await/1)

    # # where_bigfoot = Fetcher.get_result(pid4)
    # where_bigfoot = Task.await(task)

      # [
      #   Fetcher.async(fn -> VideoCam.get_snapshot("cam-1") end),
      #   Fetcher.async(fn -> VideoCam.get_snapshot("cam-2") end),
      #   Fetcher.async(fn -> VideoCam.get_snapshot("cam-3") end)
      # ]

    # Fetcher.async("cam-1")
    # Fetcher.async("cam-2")
    # Fetcher.async("cam-3")

    # snapshot1 = Fetcher.get_result(pid1)
    # snapshot2 = Fetcher.get_result(pid2)
    # snapshot3 = Fetcher.get_result(pid3)

    # spawn(fn -> send(parent, {:result, VideoCam.get_snapshot("cam-1")}) end)
    # spawn(fn -> send(parent, {:result, VideoCam.get_snapshot("cam-2")}) end)
    # spawn(fn -> send(parent, {:result, VideoCam.get_snapshot("cam-3")}) end)

    # snapshot1 = receive do {:result, filename} -> filename end
    # snapshot2 = receive do {:result, filename} -> filename end
    # snapshot3 = receive do {:result, filename} -> filename end

    # snapshot1 = VideoCam.get_snapshot("cam-1")
    # snapshot2 = VideoCam.get_snapshot("cam-2")
    # snapshot3 = VideoCam.get_snapshot("cam-3")

    # snapshots = [snapshot1, snapshot2, snapshot3]

  def route(%Conv{ method: "GET", path: "/kaboom" } = _conv) do
    raise "Kaboom!"
  end

  def route(%Conv{ method: "GET", path: "/hibernate/" <> time } = conv) do
    time |> String.to_integer |> :timer.sleep

    %{ conv | status: 200, resp_body: "Awake!" }
  end

  def route(%Conv{ method: "GET", path: "/wildthings" } = conv) do
    %{ conv | status: 200, resp_body: "Bears, Lions, Tigers" }
  end

  def route(%Conv{ method: "GET", path: "/api/bears" } = conv) do
    Servy.Api.BearController.index(conv)
  end

  def route(%Conv{ method: "GET", path: "/bears" } = conv) do
    BearController.index(conv)
  end

  def route(%Conv{ method: "GET", path: "/bears/" <> id } = conv) do
    params = Map.put(conv.params, "id", id)
    BearController.show(conv, params)
  end

  # name=Baloo&type=Brown
  def route(%Conv{ method: "POST", path: "/bears" } = conv) do
    BearController.create(conv, conv.params)
  end

  def route(%Conv{ method: "GET", path: "/about" } = conv) do
      @pages_path
      |> Path.join("about.html")
      |> File.read
      |> handle_file(conv)
  end

  def route(%Conv{ path: path} = conv) do
    %{ conv | status: 404, resp_body: "No #{path} here!" }
  end

  def handle_file({:ok, content}, conv) do
    %{ conv | status: 200, resp_body: content}
  end

  def handle_file({:error, :enoent}, conv) do
    %{ conv | status: 404, resp_body: "File not found"}
  end

  def handle_file({:error, reason}, conv) do
    %{ conv | status: 500, resp_body: "File error: #{reason}"}
  end

  # def route(%{ method: "GET", path: "/about"} = conv) do
  #   file =
  #     Path.expand("../pages", __DIR__)
  #     |> Path.join("about.html")

  #   case File.read(file) do
  #     {:ok, content} ->
  #       %{ conv | status: 200, resp_body: content}

  #     {:error, :enoent} ->
  #       %{ conv | status: 404, resp_body: "File not found"}

  #     {:error, reason} ->
  #       %{ conv | status: 500, resp_body: "File error: #{reason}"}
  #   end
  # end

  def format_response(%Conv{} = conv) do
    """
    HTTP/1.1 #{Conv.full_status(conv)}\r
    Content-Type: #{conv.resp_content_type}\r
    Content-Length: #{String.length(conv.resp_body)}\r
    \r
    #{conv.resp_body}
    """
  end
end

# path_url = IO.gets("Enter path: ") |> String.trim
# method_url = IO.gets("Enter method: ") |> String.trim

# request = """
#   #{method_url} #{path_url} HTTP/1.1
#   Host: example.com
#   User-Agent: ExampleBrowser/1.0
#   Accept: */*
#   Content-Type: application/x-www-form-urlencoded
#   Content-Length: 21

#   name=Baloo&type=Brown
#   """

# response = Servy.Handler.handle(request)
# IO.puts(response)

_request = """
  GET /bears HTTP/1.1
  Host: example.com
  User-Agent: ExampleBrowser/1.0
  Accept: */*
  Content-Type: application/x-www-form-urlencoded
  Content-Length: 21

  name=Baloo&type=Brown
  """
