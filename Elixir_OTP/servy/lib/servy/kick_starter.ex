defmodule Servy.KickStarter do
  use GenServer

  # Video 29b
  def start_link(_args) do
    IO.puts("Starting the kickstarter...")
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  # def start do
  #   IO.puts("Starting the kickstarter...")
  #   GenServer.start(__MODULE__, :ok, name: __MODULE__)
  # end

  def init(:ok) do
    # Trap exit signals to prevent KickStarter from crashing wjen linked server such as Http Server crashes
    # Converts the exit signal as message which is filtered by GenServer as unexpected message so there is a need to override the handle_info
    Process.flag(:trap_exit, true)

    # Start the Http Server
    server_pid = start_server()

    # IO.puts("Starting the HTTP server...")
    # server_pid = spawn(Servy.HttpServer, :start, [4000])
    # Process.link(server_pid)
    # Process.register(server_pid, :http_server)

    {:ok, server_pid}
  end

  def handle_info({:EXIT, _pid, reason}, _state) do
    IO.puts("HttpServer exited (#{inspect reason})")

    # Restart HttpServer
    server_pid = start_server()

    # IO.puts("Starting the HTTP server...")
    # server_pid = spawn(Servy.HttpServer, :start, [4000])
    # Process.link(server_pid)
    # Process.register(server_pid, :http_server)

    {:noreply, server_pid}
  end

  defp start_server do
    IO.puts("Starting the HTTP server...")

    # Video 30
    # Get the port from the application environment
    port = Application.get_env(:servy, :port)

    # server_pid = spawn(Servy.HttpServer, :start, [4000])
    # Process.link(server_pid)

    # Shortcut to the two commented code above to automatically link a spawn process to another process
    server_pid = spawn_link(Servy.HttpServer, :start, [port])
    Process.register(server_pid, :http_server)
    server_pid
  end

end
