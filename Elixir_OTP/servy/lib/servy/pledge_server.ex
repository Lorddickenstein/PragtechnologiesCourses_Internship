defmodule Servy.PledgeServer do

  @name :pledge_server

  # Inject default implementations of callback functions in this module
  use GenServer

  defmodule State do
    defstruct cache_size: 3, pledges: []
  end

  # def child_spec(_arg) do
  #   %{
  #     id: Servy.PledgeServer,
  #     restart: :temporary,
  #     shutdown: 5000,
  #     start: {Servy.PledgeServer, :start_link, [[]]},
  #     type: :worker
  #   }
  # end

  # Client Interface

  # Video 29a
  def start_link(_args) do
    IO.puts("Starting the pledge server...")
    # GenServer.start(__MODULE__, [], name: @name)
    # GenServer.start(__MODULE__, %{cache_size: 3, pledges: []}, name: @name)
    GenServer.start_link(__MODULE__, %State{}, name: @name)
  end

  # def start do
  #   IO.puts(" Starting the pledge server...")
  #   # GenServer.start(__MODULE__, [], name: @name)
  #   # GenServer.start(__MODULE__, %{cache_size: 3, pledges: []}, name: @name)
  #   GenServer.start(__MODULE__, %State{}, name: @name)
  # end

  # def create_pledge(name, amount) do
  #   send @name, {self(), :create_pledge, name, amount}

  #   receive do {:response, status} -> status end
  # end

  # def recent_pledges do
  #   # Returns the most recent pledges (cache):
  #   send @name, {self(), :recent_pledges}

  #   receive do {:response, pledges} -> pledges end
  # end

  # def total_pledged do
  #   # Returns the most total pledges (cache):
  #   send @name, {self(), :total_pledged}

  #   receive do {:response, total} -> total end
  # end

  def create_pledge(name, amount) do
    GenServer.call @name, {:create_pledge, name, amount}
  end

  def recent_pledges do
    GenServer.call @name, :recent_pledges
  end

  def total_pledged do
    GenServer.call @name, :total_pledged
  end

  def clear do
    GenServer.cast @name, :clear
  end

  def set_cache_size(size) do
    GenServer.cast @name, {:set_cache_size, size}
  end

    # Server Callbacks

    def init(state) do
      pledges = fetch_recent_pledges_from_service()
      new_state = %{ state | pledges: pledges}
      {:ok, new_state}
    end

  # Modify to match the GenServer's callback functions
  def handle_call(:total_pledged, _from, state) do
    total = Enum.map(state.pledges, &elem(&1, 1)) |> Enum.sum
    {:reply, total, state}
  end

  def handle_call(:recent_pledges, _from, state) do
    {:reply, state.pledges, state}
  end

  def handle_call({:create_pledge, name, amount}, _from, state) do
    {:ok, id} = send_pledge_to_service(name, amount)
    mose_recent_pledges = Enum.take(state.pledges, state.cache_size - 1)
    cached_pledges = [ {name, amount} | mose_recent_pledges ]
    new_state = %{ state | pledges: cached_pledges}
    {:reply, id, new_state}
  end

  def handle_cast(:clear, state) do
    {:noreply, %{ state | pledges: []}}
  end

  def handle_cast({:set_cache_size, size}, state) do
    new_state = %{ state | cache_size: size}
    {:noreply, new_state}
  end

  def handle_info(message, state) do
    IO.puts "Can't touch this! #{inspect message}"
    {:noreply, state}
  end

  defp send_pledge_to_service(_name, _amount)  do
    # CODE GOES HERE TO SEND PLEDGE TO EXTERNAL SOURCE
    {:ok, "pledge-#{:rand.uniform(1000)}"}
  end

  defp fetch_recent_pledges_from_service do
    # CODE GOES HERE TO FETCH RECENT PLEDGES FROM EXTERNAL SERVICE

    # Example return value:
    [ {"wilma", 15}, {"fred", 25} ]
  end

  # def handle_call(:total_pledged, state) do
  #   total = Enum.map(state, &elem(&1, 1)) |> Enum.sum
  #   {total, state}
  # end

  # def handle_call(:recent_pledges, state) do
  #   {state, state}
  # end

  # def handle_call({:create_pledge, name, amount}, state) do
  #   {:ok, id} = send_pledge_to_service(name, amount)
  #   mose_recent_pledges = Enum.take(state, 2)
  #   new_state = [ {name, amount} | mose_recent_pledges ]
  #   {id, new_state}
  # end

  # def handle_cast(:clear, _state) do
  #   []
  # end

  # def create_pledge(name, amount) do
  #   {:ok, id} = send_pledge_to_service(name, amount)

  #   # Cache the pledge:
  #    [ {"larry", 10} ]
  # end

  # def recent_pledges do
  #   # Returns the most recent pledges (cache):
  #   [ {"larry", 10} ]
  # end
end

# alias Servy.PledgeServer

# {:ok, pid} = Servy.PledgeServer.start()

# send pid, {:stop, "hammertime!"}

# handle_info is a callback function that is automatically called to handle any messages that aren't call or cast

# PledgeServer.set_cache_size(4)

# IO.inspect PledgeServer.create_pledge("larry", 10)

# PledgeServer.clear()

# IO.inspect PledgeServer.create_pledge("moe", 20)
# IO.inspect PledgeServer.create_pledge("curly", 30)
# IO.inspect PledgeServer.create_pledge("daisy", 40)
# IO.inspect PledgeServer.create_pledge("grace", 50)

# IO.inspect PledgeServer.recent_pledges()

# IO.inspect PledgeServer.total_pledged()

# IO.inspect Process.info(pid, :messages)
# alias Servy.PledgeServer

# pid = spawn(PledgeServer, :listen_loop, [[]])

# send pid, {:create_pledge, "larry", 10}
# send pid, {:create_pledge, "moe", 20}
# send pid, {:create_pledge, "curly", 30}
# send pid, {:create_pledge, "daisy", 40}
# send pid, {:create_pledge, "grace", 50}

# send pid, {self(), :recent_pledges}
# receive do {:response, pledges} -> IO.inspect(pledges) end

# curl -XPOST http://localhost:4000/pledges -d 'name=larry&amount=100'
