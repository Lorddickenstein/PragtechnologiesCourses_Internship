defmodule Servy.GenericServer do

  def start(callback_module, initial_state, name) do
    # IO.puts(" Starting the pledge server...")
    pid = spawn(__MODULE__, :listen_loop, [initial_state, callback_module])
    Process.register(pid, name)
    pid
  end

  # Helper Functions

  def call(pid, message) do
    send pid, {:call, self(), message}

    receive do {:response, response} -> response end
  end

  def cast(pid, message) do
    send pid, message
  end

  def listen_loop(state, callback_module) do
    # IO.puts("\n Waiting for a message... ")
    receive do

      {:call, sender, message} when is_pid(sender)->
        {response, state} = callback_module.handle_call(message, state)
        send sender, {:response, response}
        listen_loop(state, callback_module)

      {:cast, message} ->
        new_state = callback_module.handle_cast(message, state)
        listen_loop(new_state, callback_module)

      # Handle rogue requests
      unexpected ->
        IO.puts("Unexpected message: #{inspect unexpected}")
        listen_loop(state, callback_module)

      # {sender, {:create_pledge, name, amount}} ->
      #   {:ok, id} = send_pledge_to_service(name, amount)
      #   mose_recent_pledges = Enum.take(state, 2)
      #   new_state = [ {name, amount} | mose_recent_pledges ]
      #   # IO.puts("#{name} pledged #{amount}!")
      #   # IO.puts("New state is #{inspect new_state}")
      #   send sender, {:response, id}
      #   listen_loop(new_state)
      # {sender, :recent_pledges} ->
      #   send sender, {:response, state}
      #   # IO.puts("Sent pledges to #{inspect sender}")
      #   listen_loop(state)
      # {sender, :total_pledged} ->
      #   total = Enum.map(state, &elem(&1, 1)) |> Enum.sum
      #   send sender, {:response, total}
      #   listen_loop(state)
    end
  end
end

defmodule Servy.PledgeServerHandRolled do

  @name :pledge_server_process

  alias Servy.GenericServer

  # Client Interface

  def start do
    IO.puts(" Starting the pledge server...")
    GenericServer.start(__MODULE__, [], @name)
  end

  # def create_pledge(name, amount) do
  #   send @name, {self(), :create_pledge, name, amount}

  #   receive do {:response, status} -> status end
  # end

  # def recent_pledges do
  #   # Returns the most recent pledges (cache):
  #   send @name, {self(), :recent_pledges}

  #   receive do {:response, pledges} -> pledges end
  # end

  # def total_pledged do
  #   # Returns the most total pledges (cache):
  #   send @name, {self(), :total_pledged}

  #   receive do {:response, total} -> total end
  # end

  def create_pledge(name, amount) do
    GenericServer.call @name, {:create_pledge, name, amount}
  end

  def recent_pledges do
    GenericServer.call @name, :recent_pledges
  end

  def total_pledged do
    GenericServer.call @name, :total_pledged
  end

  def clear do
    GenericServer.cast @name, :clear
  end

    # Server Callbacks

  def handle_call(:total_pledged, state) do
    total = Enum.map(state, &elem(&1, 1)) |> Enum.sum
    {total, state}
  end

  def handle_call(:recent_pledges, state) do
    {state, state}
  end

  def handle_call({:create_pledge, name, amount}, state) do
    {:ok, id} = send_pledge_to_service(name, amount)
    mose_recent_pledges = Enum.take(state, 2)
    new_state = [ {name, amount} | mose_recent_pledges ]
    {id, new_state}
  end

  def handle_cast(:clear, _state) do
    []
  end

  defp send_pledge_to_service(_name, _amount)  do
    # CODE GOES HERE TO SEND PLEDGE TO EXTERNAL SOURCE
    {:ok, "pledge-#{:rand.uniform(1000)}"}
  end

  # def create_pledge(name, amount) do
  #   {:ok, id} = send_pledge_to_service(name, amount)

  #   # Cache the pledge:
  #    [ {"larry", 10} ]
  # end

  # def recent_pledges do
  #   # Returns the most recent pledges (cache):
  #   [ {"larry", 10} ]
  # end
end

# alias Servy.PledgeServerHandRolled

# pid = Servy.PledgeServerHandRolled.start()

# send pid, {:stop, "hammertime!"}

# IO.inspect PledgeServerHandRolled.create_pledge("larry", 10)
# IO.inspect PledgeServerHandRolled.create_pledge("moe", 20)
# IO.inspect PledgeServerHandRolled.create_pledge("curly", 30)
# IO.inspect PledgeServerHandRolled.create_pledge("daisy", 40)

# PledgeServerHandRolled.clear()

# IO.inspect PledgeServerHandRolled.create_pledge("grace", 50)

# IO.inspect PledgeServerHandRolled.recent_pledges()

# IO.inspect PledgeServerHandRolled.total_pledged()

# IO.inspect Process.info(pid, :messages)
# alias Servy.PledgeServerHandRolled

# pid = spawn(PledgeServerHandRolled, :listen_loop, [[]])

# send pid, {:create_pledge, "larry", 10}
# send pid, {:create_pledge, "moe", 20}
# send pid, {:create_pledge, "curly", 30}
# send pid, {:create_pledge, "daisy", 40}
# send pid, {:create_pledge, "grace", 50}

# send pid, {self(), :recent_pledges}
# receive do {:response, pledges} -> IO.inspect(pledges) end

# curl -XPOST http://localhost:4000/pledges -d 'name=larry&amount=100'
