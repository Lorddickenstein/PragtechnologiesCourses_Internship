defmodule Servy.SensorServer do

  @name :sensor_server
  # @refresh_interval :timer.minutes(60) # :timer.seconds(5)

  use GenServer

  defmodule State do
    defstruct interval: 60, state: %{}
  end

  # Video 29a
  def start_link(interval) do
    IO.puts("Starting the sensor server with #{interval} min refresh...")
    state = set_interval(%State{}, interval)
    GenServer.start_link(__MODULE__, state, name: @name)
  end

  # def start do
  #   GenServer.start(__MODULE__, %{}, name: @name)
  # end

  def get_sensor_data do
    GenServer.call @name, :get_sensor_data
  end

  # Server Callbacks

  def init(state) do
    initial_state = run_tasks_to_get_sensor_data()
    # Schedule a process into the Future
    schedule_refresh(state)
    {:ok, %{state | state: initial_state}}
  end

  def handle_info(:refresh, state) do
    IO.puts("Refreshing the cache...")
    new_state = run_tasks_to_get_sensor_data()
    schedule_refresh(state)
    {:noreply, %{state | state: new_state}}
  end

  defp schedule_refresh(state) do
    Process.send_after(self(), :refresh, state.interval)
  end

  def handle_call(:get_sensor_data, _from, state) do
    { :reply, state, state }
  end

  defp run_tasks_to_get_sensor_data do
    IO.puts("Running tasks to get sensor data...")

    task = Task.async(fn -> Servy.Tracker.get_location("bigfoot") end)

    snapshots =
      ["cam-1", "cam-2", "cam-3"]
      |> Enum.map(&Task.async(fn -> Servy.VideoCam.get_snapshot(&1) end))
      |> Enum.map(&Task.await/1)

    where_is_bigfoot = Task.await(task)

    IO.puts(inspect snapshots)

    %{ snapshots: snapshots, location: where_is_bigfoot }
  end

  defp set_interval(state, interval) do
    %{ state | interval: :timer.seconds(interval) }
  end
end
