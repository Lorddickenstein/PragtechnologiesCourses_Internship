defmodule Servy.ServicesSupervisor do

  # An OTP Behaviour called supervises other processes, which is refered as child processes, even supervisors
  # When using supervisor behaviour, the default child_spec() function is automatically injected to the module
  use Supervisor

  def start_link(_args) do
    IO.puts("Starting the services supervisor...")

    # start_link - spawn a supervisor process and links it to function that calls start_link
    Supervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do

    # In init is where the supervisor is told which child processes to supervise
    children = [
      Servy.PledgeServer,
      {Servy.SensorServer, 1}
    ]

    Supervisor.init(children, strategy: :one_for_one)
    # Restart Strategies:
      # :one_for_all - When one process terminates, all others are terminated by the supervisor and restarted
      # :one_for_one - Terminates and restarts only one process
  end
end

# Supervisor.which_children(sup_pid) - returns a list of child processes handled by the supervisor
# Supervisor.count_children(sup_pid) - returns a map of status of how many children, supervisors, specs, active, the supervisor is handling
