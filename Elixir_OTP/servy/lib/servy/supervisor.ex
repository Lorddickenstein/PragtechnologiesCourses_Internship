defmodule Servy.Supervisor do

  use Supervisor

  def start_link do
    IO.puts("Starting the toplevel supervisor...")
    Supervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    children = [
      Servy.KickStarter,
      Servy.ServicesSupervisor
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end

# Video 29
# Process.whereis(Servy.ServicesSupervisor) |> Process.exit(:kill)
# :kill is an untrappable exit signal which is needed to crash a supervisor because it traps any other messages

# terminating the Supervisor will restart the Supervisor Server but also its children

# Video 30
# mix clean - clean up all the build artifacts or compiled code
# mix compile - compile all .ex files
# Application.started_applications() - shows running applications
  # Running iex -S mix automatically started the servy application and all its dependencies which are also application
# Application.stop(:servy) - terminates the servy application
# Application.start(:servy) - starts the servy application
