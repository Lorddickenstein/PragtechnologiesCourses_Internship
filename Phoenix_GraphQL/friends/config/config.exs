import Config

config :friends, Friends.Repo,
  database: "friends_repo",
  username: "postgres",
  password: "postgres",
  hostname: "localhost"

  # Define what port to connect into (default 5432)
  # port: 15432

config :friends, ecto_repos: [Friends.Repo]
